import React, { useState } from "react";
import logo from "../assets/img/brand/logo3.png";
import axios  from 'axios';
import { useHistory } from 'react-router-dom'

const Auth = () => {
  const history = useHistory();
  const [compteDetail, setCompteDetail] = useState({
    username: "",
    password: "",
  });
  

  const halndChange=(event,field)=>{
    let valuestart=event.target.value
    setCompteDetail({...compteDetail,[field]:valuestart})

  }

  const submitValue= async (e)=>{
    e.preventDefault();
    await axios.post("http://localhost:8280/login",{
      username:compteDetail.username,
      password:compteDetail.password
    }).then((res)=>{
      
       history.push('/admin/index')
    }
    )

    

    
    

  }
  return (
    <>
      <section>
        <div className="mt-4 ml-6">
          <img style={{ width: "130px" }} alt="not exits" src={logo} />
        </div>
        <div className="container mt-5 pt-5">
          <div className="row">
            <div className="col-12 col-sm-7 col-md-6 m-auto">
              <div className="card border-0 shadow">
                <div className="card-body text-center">
                  <h1 className="pb-4">Authentification</h1>
                  <form action="" onSubmit={submitValue}>
                    <input
                      type="text"
                      name=""
                      value={compteDetail.username}
                      onChange={(e)=>halndChange(e,"username")}
                      className="form-control my-4 py-2"
                      placeholder="Username"
                    />
                    <input
                      type="password"
                      name=""
                      value={compteDetail.password}
                      onChange={(e)=>halndChange(e,"password")}
                      className="form-control my-4 py-2"
                      placeholder="Password"

                    />
                    <div className="text-center mt-3">
                      <button type="submit" className="btn btn-primary">
                        Login
                      </button>
                      {/* <a href="#" className="nav-link">Already have an account ?</a> */}
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default Auth;
