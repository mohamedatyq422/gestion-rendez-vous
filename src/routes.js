
import Index from "views/Index.js";
import Profile from "views/examples/Conges.js";
import Maps from "views/examples/RendezVous";
import Register from "views/examples/GestionPatient.js";
import Login from "views/examples/Login.js";
import Icons from "views/examples/Patient.js";


var routes = [
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-tv-2 text-primary",
    component: Index,
    layout: "/admin"
  },
  {
    path: "/patient",
    name: "Patient",
    icon: "ni ni-single-02 text-primary",
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/rendez-vous",
    name: "Prenez rendez-vous",
    icon: "ni ni-calendar-grid-58 text-primary",
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/Mes congés",
    name: "Mes congés",
    icon: "ni ni-paper-diploma text-primary",
    component: Profile,
    layout: "/admin"
  },
  {
    path: "/gestionPatient",
    name: "Gestion Patients",
    icon: "ni ni-circle-08 text-primary",
    component: Register,
    layout: "/admin"
  },
  {
    path: "/login",
    name: "Se déconnecter",
    icon: "ni ni-button-power text-primary",
    component: Login,
    layout: "/auth"
  },
  // {
  //   path: "/tables",
  //   name: "Tables",
  //   icon: "ni ni-bullet-list-67 text-red",
  //   component: Tables,
  //   layout: "/admin"
  // },
  
  
];
export default routes;
