import Head from "components/Head/Head";
import React, { useState } from "react";
import { FaRegCalendarCheck, FaEdit, FaRegTrashAlt } from "react-icons/fa";
import {
  Card,
  CardFooter,
  CardHeader,
  Container,
  Modal,
  ModalBody,
  ModalHeader,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
} from "reactstrap";
export default function Icons(args) {
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);
  return (
    <>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>
          <h2>Ajouter un patinet</h2>
        </ModalHeader>
        <ModalBody>
          <form className="col g-2 needs-validation">
            <div className="form-floating mb-3">
              <label for="validationTooltip01" className="form-label">
                CIN
              </label>
              <input
                type="text"
                className="form-control"
                id="validationTooltip01"
                required
              />
            </div>
            <div className="form-floating mb-3">
              <label for="validationTooltip01" className="form-label">
                Nom
              </label>
              <input
                type="text"
                className="form-control"
                id="validationTooltip01"
                required
              />
            </div>
            <div className="form-floating mb-3">
              <label for="validationTooltip01" className="form-label">
                Prenom
              </label>
              <input
                type="text"
                className="form-control"
                id="validationTooltip01"
                required
              />
            </div>
            <div className="form-floating mb-3">
              <label for="validationTooltip01" className="form-label">
                Date naissance
              </label>
              <input
                type="date"
                className="form-control"
                id="validationTooltip01"
                required
              />
            </div>
            <div className="form-floating mb-3">
              <label for="validationTooltip01" className="form-label">
                Telephone
              </label>
              <input
                type="tel"
                className="form-control"
                id="validationTooltip01"
                required
              />
            </div>
            <div className="form-floating mb-3">
              <label for="validationTooltip01" className="form-label">
                Adress
              </label>
              <input
                type="text"
                className="form-control"
                id="validationTooltip01"
                required
              />
            </div>

            <div className="col-12">
              <button className="btn btn-primary ml--3" type="submit">
                Ajouter
              </button>
            </div>
          </form>
        </ModalBody>
      </Modal>

      <Head />
      <br />

      <Container className="mt-3" fluid>
        <Card className="shadow mb-5 ">
          <CardHeader className="border-0 d-flex justify-content-between">
            <h2 className="mb-0">Nouveau Patient</h2>
            <button className="btn btn-primary" onClick={toggle}>
              Ajouter
            </button>
          </CardHeader>
        </Card>
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <h3 className="mb-0">Table Patients</h3>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">CIN</th>
                    <th scope="col">nom</th>
                    <th scope="col">prenom</th>
                    <th scope="col">date naissance</th>
                    <th scope="col">telephone</th>
                    <th scope="col">adress</th>
                    <th scope="col">action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>BB191650</td>
                    <td>Atyq</td>
                    <td>Mohamed</td>
                    <td>28/10/2001</td>
                    <td>0689206252</td>
                    <td>Casablanca</td>
                    <td>
                      <button className="btn btn-outline-danger px-2 py-2">
                        <FaRegTrashAlt style={{ fontSize: "19px" }} />
                      </button>
                      <button className="btn btn-outline-info px-2 py-2">
                        <FaEdit style={{ fontSize: "19px" }} />
                      </button>
                      <button className="btn btn-outline-primary px-2 py-2">
                        <FaRegCalendarCheck style={{ fontSize: "19px" }} />
                      </button>
                    </td>
                  </tr>
                </tbody>
              </Table>
              <CardFooter className="py-4">
                <nav aria-label="...">
                  <Pagination
                    className="pagination justify-content-end mb-0"
                    listClassName="justify-content-end mb-0"
                  >
                    <PaginationItem className="disabled">
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                        tabIndex="-1"
                      >
                        <i className="fas fa-angle-left" />
                        <span className="sr-only">Previous</span>
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem className="active">
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        1
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        2 <span className="sr-only">(current)</span>
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        3
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        <i className="fas fa-angle-right" />
                        <span className="sr-only">Next</span>
                      </PaginationLink>
                    </PaginationItem>
                  </Pagination>
                </nav>
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
}
