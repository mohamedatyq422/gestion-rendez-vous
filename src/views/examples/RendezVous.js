import React from "react";
import { Card, CardBody, CardHeader, Container } from "reactstrap";
import Head from "components/Head/Head";

const Maps = () => {
  return (
    <>
      <Head />
      <Container fluid>
        <Card className="mt-5">
          <CardHeader><h2>Rendez-vous</h2></CardHeader>
          <CardBody  className="d-flex justify-content-center">
            <form>
              <label>Date rendez-vous :</label><br/>
              <input className="w-auto" type="datetime-local" required/>
              <div className="d-flex justify-content-between w-auto mt-4">
                <div>
                  <span>Cabinet </span>
                <input type="checkbox"/>
                </div>
                <div>
                  <span>Externe </span>
                <input type="checkbox"/>
                </div>
              </div>
              <button className="w-auto mt-5 ml-6 btn btn-primary" type="submit">Planifiez</button>
            </form>
          </CardBody>
        </Card>
      </Container>
    </>
  );
};

export default Maps;
