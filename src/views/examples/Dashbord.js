/*!

=========================================================
* Argon Dashboard React - v1.2.2
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  CardFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  Table,
  Container,
  Row,
} from "reactstrap";
import { FaRegEye, FaRegTrashAlt } from "react-icons/fa";
const Tables = () => {
  return (
    <>
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <h3 className="mb-0">Table rendez-vous</h3>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Patient</th>
                    <th scope="col">Date Rendez-Vous</th>
                    <th scope="col">Status</th>
                    <th scope="col">Source</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>
                      <Badge color="" className="badge-dot mr-4">
                        <i className="bg-success" />
                        Mohamed
                      </Badge>
                    </td>
                    <td>01/04/2021</td>
                    <td>Confirmé</td>
                    <td>Cabiniet</td>
                    <td>
                      <button className="btn btn-outline-danger px-2 py-2">
                        <FaRegTrashAlt style={{ fontSize: "19px" }} />
                      </button>
                      <button className="btn btn-outline-info px-2 py-2">
                        <FaRegEye style={{ fontSize: "19px" }} />
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>
                      <Badge color="" className="badge-dot mr-4">
                        <i className="bg-success" />
                        Atyq
                      </Badge>
                    </td>
                    <td>01/04/2021</td>
                    <td>Confirmé</td>
                    <td>Cabiniet</td>
                    <td>
                      <button className="btn btn-outline-danger px-2 py-2">
                        <FaRegTrashAlt style={{ fontSize: "19px" }} />
                      </button>
                      <button className="btn btn-outline-info px-2 py-2">
                        <FaRegEye style={{ fontSize: "19px" }} />
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>
                      <Badge color="" className="badge-dot mr-4">
                        <i className="bg-success" />
                        Adil
                      </Badge>
                    </td>
                    <td>01/04/2021</td>
                    <td>Confirmé</td>
                    <td>Cabiniet</td>
                    <td>
                      <button className="btn btn-outline-danger px-2 py-2">
                        <FaRegTrashAlt style={{ fontSize: "19px" }} />
                      </button>
                      <button className="btn btn-outline-info px-2 py-2">
                        <FaRegEye style={{ fontSize: "19px" }} />
                      </button>
                    </td>
                  </tr>
                </tbody>
              </Table>
              <CardFooter className="py-4">
                <nav aria-label="...">
                  <Pagination
                    className="pagination justify-content-end mb-0"
                    listClassName="justify-content-end mb-0"
                  >
                    <PaginationItem className="disabled">
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                        tabIndex="-1"
                      >
                        <i className="fas fa-angle-left" />
                        <span className="sr-only">Previous</span>
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem className="active">
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        1
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        2 <span className="sr-only">(current)</span>
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        3
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        <i className="fas fa-angle-right" />
                        <span className="sr-only">Next</span>
                      </PaginationLink>
                    </PaginationItem>
                  </Pagination>
                </nav>
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Tables;
