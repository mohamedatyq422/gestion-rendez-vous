import Head from "components/Head/Head";
import {
  Card,
  CardHeader,
  Row,
  Table,
  Container,
  PaginationItem,
  PaginationLink,
  Pagination,
  CardFooter,
} from "reactstrap";
import { FaRegEye, FaRegTrashAlt } from "react-icons/fa";
const Register = () => {
  return (
    <>
      <Head />
      <Container className="mt-6" fluid>
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="d-flex justify-content-between">
                <input
                  class="form-control me-2 w-25"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                />
                <div>
                  <h3>
                    Nombre des Patients :{" "}
                    <span style={{ color: "green", paddingLeft: "14px" }}>
                      {" "}
                      123
                    </span>
                  </h3>
                </div>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="bg-dark text-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">CIN</th>
                    <th scope="col">nom</th>
                    <th scope="col">prenom</th>
                    <th scope="col">date naissance</th>
                    <th scope="col">telephone</th>
                    <th scope="col">adress</th>
                    <th scope="col">Date Rendez-Vous</th>
                    <th scope="col">Status</th>
                    <th scope="col">Source</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>BB11452</td>
                    <td>Atyq</td>
                    <td>Mohamed</td>
                    <td>12/04/1987</td>
                    <td>0782342101</td>
                    <td>Casablanca</td>
                    <td>01/04/2021</td>
                    <td>Confirmé</td>
                    <td>Cabiniet</td>
                    <td>
                      <button className="btn btn-outline-danger px-2 py-2">
                        <FaRegTrashAlt style={{ fontSize: "19px" }} />
                      </button>
                      <button className="btn btn-outline-info px-2 py-2">
                        <FaRegEye style={{ fontSize: "19px" }} />
                      </button>
                    </td>
                  </tr>
                  <tr>
                  <td>2</td>
                    <td>BB11452</td>
                    <td>rami</td>
                    <td>adil</td>
                    <td>12/04/1987</td>
                    <td>0782342101</td>
                    <td>Casablanca</td>
                    <td>01/04/2021</td>
                    <td>Confirmé</td>
                    <td>Cabiniet</td>
                    <td>
                      <button className="btn btn-outline-danger px-2 py-2">
                        <FaRegTrashAlt style={{ fontSize: "19px" }} />
                      </button>
                      <button className="btn btn-outline-info px-2 py-2">
                        <FaRegEye style={{ fontSize: "19px" }} />
                      </button>
                    </td>
                  </tr>
                  <tr>
                  <td>3</td>
                    <td>BB11452</td>
                    <td>Atyq</td>
                    <td>Zakaria</td>
                    <td>12/04/2001</td>
                    <td>0784387521</td>
                    <td>fes</td>
                    <td>01/04/2021</td>
                    <td>Confirmé</td>
                    <td>Cabiniet</td>
                    
                    <td>
                      <button className="btn btn-outline-danger px-2 py-2">
                        <FaRegTrashAlt style={{ fontSize: "19px" }} />
                      </button>
                      <button className="btn btn-outline-info px-2 py-2">
                        <FaRegEye style={{ fontSize: "19px" }} />
                      </button>
                    </td>
                  </tr>
                </tbody>
              </Table>
              <CardFooter className="py-4">
                <nav aria-label="...">
                  <Pagination
                    className="pagination justify-content-end mb-0"
                    listClassName="justify-content-end mb-0"
                  >
                    <PaginationItem className="disabled">
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                        tabIndex="-1"
                      >
                        <i className="fas fa-angle-left" />
                        <span className="sr-only">Previous</span>
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem className="active">
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        1
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        2 <span className="sr-only">(current)</span>
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        3
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        <i className="fas fa-angle-right" />
                        <span className="sr-only">Next</span>
                      </PaginationLink>
                    </PaginationItem>
                  </Pagination>
                </nav>
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Register;
